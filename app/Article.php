<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    public static function boot()
    {
        parent::boot();

        Article::deleting(function($article)
        {
            $article->categories()->detach();
        });

    }

    public function urlslug() {
        $slug = url('cleaning-articles/'.$this->id . '/'. $this->maincategory->slug . '/' . $this->slug);

        return $slug;
    }


    public function maincategory() {
        return $this->belongsTo('App\Category','main_category_id');
    }


    public function categories() {
        return $this->belongsToMany('App\Category','article_category');
    }




}
