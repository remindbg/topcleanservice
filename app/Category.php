<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function articles() {
        return $this->belongsToMany('App\Article','article_category');
    }

    public function urlslug() {
        $slug = url('cleaning-articles/'.$this->id . '/'. $this->slug . '/');

        return $slug;
    }

    public function parent() {
        return $this->belongsTo(static::class, 'parent_id');
    }

    //each category might have multiple children

    public function childrens() {
        return $this->hasMany(static::class, 'parent_id')->orderBy('name', 'asc');
    }
}
