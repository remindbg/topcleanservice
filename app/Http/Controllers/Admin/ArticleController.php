<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\View\View;
use App\Traits\UploadTrait;

class ArticleController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $articles = Article::with('categories','maincategory')
            ->orderBy('created_at','desc')
            ->paginate(12);

        $categories = Category::all();

        return view('rpanel.blog.article.index',compact('articles','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        $categories = Category::all();
        return view('rpanel.blog.article.create',compact('categories'));

        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $article = new Article();

        $article->title = $request['title'];
        $article->slug = Str::slug($request['title'],'-');
        $article->body = $request['body'];
        $article->is_active = $request['is_active'];
        $article->main_category_id = $request['main_category_id'];
        if ($request->has('feautured_image')) {
            // Get image file
            $image = $request->file('feautured_image');
            // Make a image name based on user name and current timestamp
            $name = $article->slug;
            $folder = 'articlesrepo';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);


            $article->image =  'uploads/' . $uploaded;

            $article->image =  $uploaded;

        }
        $article->seo_title = $request['seo_title'];
        $article->seo_description = $request['seo_description'];






        $article->save();
        $article->categories()->sync($request['categories']);


        return redirect()->route('rpanel.article.index')->with('success','Article Added');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $categories = Category::all();



        return view('rpanel.blog.article.edit',compact('article','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $article = Article::find($id);

        $article->title = $request['title'];
        $article->slug =$request['slug'];
        $article->body = $request['body'];

        $article->is_active = $request['is_active'];
        $article->main_category_id = $request['main_category_id'];
        if ($request->has('feautured_image')) {
            // Get image file
            $image = $request->file('feautured_image');
            // Make a image name based on user name and current timestamp
            $name = $article->slug . rand(5,10);
            $folder = 'articlesrepo';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);


            $oldpicture = $article->image;

            $oldname = basename('uploads/'. $oldpicture);

            Storage::delete($article->image); //delete old image


            $article->image =  $uploaded;


        }
        $article->seo_title = $request['seo_title'];
        $article->seo_description = $request['seo_description'];






        $article->save();
        $article->categories()->sync($request['categories']);


        return redirect()->route('rpanel.article.index')->with('success','Article Edited');

    }


    public function destroy($id)
    {
        $article = Article::find($id);
        $oldname = basename('uploads/'. $article->image);
        Storage::delete($article->image);

        $article->delete();

        return redirect()->route('rpanel.article.index')->with('success','Article Removed!');

    }

    public function uploadImage()
    {
        $imgpath = request()->file('file')->store('articles',  'public_uploads');

        $imgpath = 'uploads/'.$imgpath;


        return response()->json(['location' => $imgpath]);
    }
}
