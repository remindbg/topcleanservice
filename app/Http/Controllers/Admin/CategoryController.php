<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\UploadTrait;
use App\Article;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
class CategoryController extends Controller
{
    use UploadTrait;
    /**
     * Display a listing of the resource.
     *
     * @return view
     */
    public function index()
    {
        $categories = Category::all();



        return view('rpanel.blog.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return view
     */
    public function create()
    {
        $categories = Category::all();


        return view('rpanel.blog.category.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse;
     */
    public function store(Request $request)
    {

        $category = new Category();

        $category->name = $request['name'];
        $category->slug = Str::slug($request['name'],'-');
        $category->description = $request['description'];

        if($request['parent_id']) {
            $category->parent_id = $request['parent_id'];
        }
        else {
            $category->parent_id = 0; // which means its root category

        }
        $category->seo_title = $request['seo_title'];
        $category->seo_description = $request['seo_description'];


        $category->save();

        return redirect()->route('rpanel.category.index')->with('success','Category Added');



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $allcats = Category::all();
        return view('rpanel.blog.category.edit',compact('category','allcats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);

        $category->name = $request['name'];
        $category->slug = $request['slug'];
        $category->description = $request['description'];
        $category->parent_id = $request['parent_id'];

        $category->seo_title = $request['seo_title'];
        $category->seo_description = $request['seo_description'];

        if ($request->has('cat_image')) {


            $image = $request->file('cat_image');
            $name = Str::slug($request['name'], '-') . '-'.Str::random(4);
            $folder = 'cats';
            // Upload image
            $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);


            Storage::delete($category->cat_image); //delete the old image

            $category->cat_image = $uploaded;
        }
        $category->save();

        return redirect()->route('rpanel.category.index')->with('success','Edited Done');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        $deleteImage = Storage::delete($category->cat_image);

        $category->delete();

        return redirect()->route('rpanel.category.index')->with('success','Removed');
    }
}
