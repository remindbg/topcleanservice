<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Traits\UploadTrait;

class ServiceController extends Controller
{
    use UploadTrait;

    public function index()
    {
        $services = Services::all();
        return view('rpanel.services.index',compact('services'));

    }


    public function create()
    {
        return view('rpanel.services.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $service = new Services();
        $service->name = $request['name'];
        $service->small_text = $request['small_text'];
        $service->slug = Str::slug($service->name,'-');
        $service->body = $request['body'];
        $service->seo_title = $request['seo_title'];
        $service->seo_description = $request['seo_description'];
        $service->is_active = $request['is_active'];
        $service->is_on_homepage = $request['is_on_homepage'];

        if ($request->has('feautured_image')) {
            // Get image file
            $image = $request->file('feautured_image');
            // Make a image name based on user name and current timestamp
            $name = $service->slug . Str::random(5);;
            $folder = 'articlesrepo';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);


            $service->image =  'uploads/' . $uploaded;

            $service->image =  $uploaded;

        }



        $service->save();
        return redirect()->route('rpanel.services.index')->with('success','Service Created');




    }


    public function show()
    {
        //
    }


    public function edit($id)
    {
        $service = Services::find($id);
        return view('rpanel.services.edit',compact('service'));

    }


    public function update(Request $request, $id)
    {
        $service = Services::find($id);
        $service->name = $request['name'];
        $service->small_text = $request['small_text'];
        $service->slug = $request['slug'];
        $service->body = $request['body'];
        $service->seo_title = $request['seo_title'];
        $service->seo_description = $request['seo_description'];
        $service->is_active = $request['is_active'];
        $service->is_on_homepage = $request['is_on_homepage'];

        if ($request->has('feautured_image')) {
            // Get image file
            $image = $request->file('feautured_image');
            // Make a image name based on user name and current timestamp
            $name = $service->slug . Str::random(5);
            $folder = 'articlesrepo';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            // Upload image
            $uploaded =  $this->uploadOne($image, $folder, 'public_uploads', $name);

            $oldpicture = $service->image;

            $oldname = basename('uploads/'. $oldpicture);

            $service->image =  'uploads/' . $uploaded;
            Storage::delete($oldpicture); //delete old image

            $service->image =  $uploaded;

        }



        $service->save();
        return redirect()->route('rpanel.services.index')->with('success','Service Edited');

    }


    public function destroy($id)
    {
        $service = Services::find($id);
        $oldname = basename('uploads/'. $service->image);
        Storage::delete($service->image);

        $service->delete();

        return redirect()->route('rpanel.services.index')->with('success','Service Removed!');
    }


}
