<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        $articles = Article::with('categories','maincategory')
            ->orderBy('created_at','desc')
            ->where('is_active',true)->get();

        return view('articles.index',compact('categories','articles'));
    }





    public function single($id,$cat_slug,$article_slug)
    {
        $article = Article::findOrFail($id);

        return view('articles.single',compact('article'));

    }




}
