<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services;
use App\Category;
use App\Article;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['_static.topmenu','_static.blogsidebar'], function
        ($view) {
            $services = Services::all();
            $categories = Category::all();

            $view->with('services',$services);
            $view->with('categories',$categories);

        });
    }
}
