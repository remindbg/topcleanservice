<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    public function urlslug() {
        $slug = url('london-services/'.$this->id . '/'. $this->slug);

        return $slug;
    }
}
