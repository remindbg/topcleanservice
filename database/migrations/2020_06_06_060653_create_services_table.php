<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('image')->nullable();
            $table->string('name');
            $table->string('slug');
            $table->longText('body')->nullable();
            $table->text('small_text')->nullable();

            $table->boolean('is_active')->default(true);
            $table->boolean('is_on_homepage')->default(true);

            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('og_image')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
