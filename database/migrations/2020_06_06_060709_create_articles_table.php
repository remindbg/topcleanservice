<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->string('image')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->longText('body')->nullable();
            $table->unsignedInteger('views')->default(1);
            $table->boolean('is_active')->default(true);

            $table->unsignedInteger('main_category_id')->nullable();


            $table->string('seo_title')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('og_image')->nullable();

        });

        Schema::create('article_category', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('article_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
