<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        DB::table('users')->insert([


            'email_verified_at'          => now(),
            'name'                       => 'admin',
            'password'                   => bcrypt('cleaningparola'), // password
            'remember_token'             => Str::random(10),
            'email'                      => 'remindbg@gmail.com',
            'is_admin'                   => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

        ]);

    }
}
