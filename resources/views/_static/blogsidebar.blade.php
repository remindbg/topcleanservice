<div class="col-md-3 col-sm-4 col-xs-8">
    <div class="sidebar row">


        <aside class="widget widget_text">
            <h3 class="widget-title">Welcome to Our Blog</h3>
            <p>Browse quality Articles about Cleaning Tips, Services and ideas for home or commercial cleaning</p>
        </aside>

        <aside class="widget widget_category">
            <h3 class="widget-title">Categories</h3>
            <ul>
                @forelse($categories as $category)
                <li><a title="{{$category->name}}" href="{{$category->urlslug()}}">{{$category->name}}</a></li>
               @empty
                @endforelse
            </ul>
        </aside>
    </div>
</div><!-- col-md-3 /- -->
