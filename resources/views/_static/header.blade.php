<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

@yield('metas')

<link rel="shortcut icon" href="{{asset('build/images/favicon.png')}}">
<link href="{{asset('build/libraries/bootstrap/bootstrap.min.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{asset('build/libraries/fuelux/jquery-ui.min.css')}}">
<linK href="{{asset('build/libraries/owl-carousel/owl.carousel.css')}}" rel="stylesheet"/>
<linK href="{{asset('build/libraries/owl-carousel/owl.theme.css')}}" rel="stylesheet"/>
<link href="{{asset('build/libraries/fonts/font-awesome.min.css')}}" rel="stylesheet"/>
<link href="{{asset('build/libraries/animate/animate.min.css')}}" rel="stylesheet"/>
<link href="{{asset('build/libraries/flexslider/flexslider.css')}}" rel="stylesheet" /> <!-- flexslider -->
<link href="{{asset('build/libraries/magnific-popup.css')}}" rel="stylesheet" />
<link href="{{asset('build/css/components.css')}}" rel="stylesheet"/>
<link href="{{asset('build/style.css')}}" rel="stylesheet"/>
<link href="{{asset('build/css/media.css')}}" rel="stylesheet"/>

<link href='http://fonts.googleapis.com/css?family=Cabin:400,400italic,500,500italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-172909300-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-172909300-2');
</script>
