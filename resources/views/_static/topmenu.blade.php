<header id="header-section" class="header-section">

    <!-- Top Header -->
    <div class="top-header">
        <!-- container -->
        <div class="container">
            <div class="row">
                <!-- col-md-6 -->
                <div class="col-md-6 col-sm-6">
                    <p><img src="{{asset('build/images/icon/thumbs-icon.png')}}" alt="thumbs-icon"/>Now We are covering all London Areas!</p>
                </div><!-- col-md-6 /- -->
                <!-- col-md-6 -->
                <div class="col-md-6 col-sm-6 text-right">
                    <p><img src="{{asset('build/images/icon/clock-icon.png')}}" alt="clock-icon"/>Working hours : Mon-sat (8.00am - 6.00PM)</p>
                </div><!-- col-md-6 /- -->
            </div>
        </div><!-- container /- -->
    </div><!-- Top Header /- -->

    <!-- Logo Block -->
    <div class="logo-block">
        <!-- container -->
        <div class="container">
            <div class="row">
                <!-- col-md-2 -->
                <div class="col-md-4 col-sm-4">
                    <h4 class="text-primary">Reliable Cleaning Services in London</h4 >
                </div><!-- col-md-2 /- -->
                <!-- col-md-4 -->
                <div class="col-md-6 col-sm-8 pull-right row">
                    <!-- col-md-7 -->
                    <div class="col-md-6 col-sm-6 col-sm-offset-2 col-md-offset-2 call-us">
                        <img src="{{asset('build/images/icon/mobile-icon.png')}}" alt="mobile-icon" />
                        <p>Call to schedule your cleaning <span><a href="tel:+447808055087">+(44) 7808 055087</a></span></p>
                    </div><!-- col-md-7 /- -->
                    <!-- col-md-5 -->

                </div><!-- col-md-4 /- -->
            </div>
        </div>
    </div><!-- Logo Block /- -->

    <!-- Menu Block -->
    <div class="menu-block">
        <div class="container">
            <div class="row">
                <!-- col-md-8 -->
                <nav class="navbar navbar-default col-md-8">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a title="Logo" href="/"><img alt="logo" src="{{asset('build/images/responsive-logo.png')}}"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a title="Homepage" href="/">Home</a></li>
                            @auth()
                                @if (auth()->user()->is_admin)
                                    <li><a title="admin" href="/myrpanel" target="_blank">Admin Panel</a></li>

                                @endif
                            @endauth
                            <li class="dropdown">
                                <a  title="Services" href="/services" class="dropdown-toggle" data-toggle="dropdown">Clean Services</a>
                                <ul class="dropdown-menu" role="menu">
                                    @forelse($services as $service)
                                        <li><a title="{{$service->name}}" href="{{$service->urlslug()}}"> {{$service->name}}</a></li>

                                    @empty

                                    @endforelse
                                        <li class="small"><a title="View All" class="small" href="/london-services">View All</a></li>

                                </ul>
                            </li>
                            <li><a title="shop" href="/shop">Shop</a></li>


                            <li class="dropdown">
                                <a title="Site Pages" href="#" class="dropdown-toggle" data-toggle="dropdown">Contact</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a title="Contact" href="/contact">Contact</a></li>
                                    <li><a title="Contact" href="/contact">Fast Quote Form</a></li>

                                </ul>
                            </li>
                            <li class="dropdown">
                                <a title="Cleaning Articles" href="#" class="dropdown-toggle" data-toggle="dropdown">Articles</a>
                                <ul class="dropdown-menu" role="menu">
                                    @forelse($categories as $category)
                                        <li><a title="{{$category->name}}" href="{{$category->urlslug()}}"> {{$category->name}}</a></li>

                                    @empty

                                    @endforelse
                                    <li class="small"><a title="View All" class="small" href="/cleaning-articles">View All</a></li>

                                </ul>
                            </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav><!-- col-md-8 /- -->
                <div class="col-md-4 quote">
                    <a  title="Free Quote" href="/contact/fastquote">Free instant quote</a>
                </div>
            </div>
        </div><!-- /.container -->
    </div><!-- Menu Block /- -->
</header><!-- Header Section /- -->
