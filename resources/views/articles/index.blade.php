@extends('layouts.app')


@section('content')
    <div id="page-banner" class="page-banner">
        <img src="{{asset('build/images/page-banner.png')}}" alt="page-banner">
        <!-- container -->
        <div class="page-detail">
            <div class="container">
                <h1 class="page-title">Cleaning Articles</h1>
                <div class="page-breadcrumb pull-right">
                    <ol class="breadcrumb">
                        <li><a title="Home" href="/">Home</a></li>
                        <li>Cleaning Articles</li>
                    </ol>
                </div>
            </div>
        </div><!-- container /- -->
    </div>
    <div id="blog-section" class="blog-section blog-list ow-section">
        <!-- container -->
        <div class="container">
            <!-- col-md-8 -->
            <div class="col-md-8 col-sm-7 no-padding">

                @forelse($articles as $article)
                    <article>
                        <div class="blog-box">
                            <div class="entry-cover">
                                <a title="Blog Cover" href="{{$article->urlslug()}}">
                                    @if ($article->image)
                                        <img src="{{asset('uploads/'.$article->image)}}"
                                             alt="{{$article->title}}">
                                        @else
                                        <img src="{{asset('build/images/blog/blog-list-3.jpg')}}"
                                             alt="{{$article->title}}">
                                    @endif

                                </a>
                                <span class="posted-on">
								<span class="like">{{$article->views}}</span>
								<span class="date">{{$article->created_at->diffForHumans()}}</span>
							</span>
                            </div>
                            <div class="blog-box-inner">
                                <!-- entry header -->
                                <header class="entry-header">
                                    <h3><a title="{{$article->title}}" href="{{$article->urlslug()}}">{{$article->title}}</a></h3>
                                </header><!-- entry header /- -->

                                <footer class="entry-footer">

                                    <span class="byline">
									<span class="screen-reader-text">Views </span>
									<a title="Views" href="#">{{$article->views}}</a>
								</span>


									<span class="screen-reader-text">{{$article->maincategory->name}} </span>


                                </footer>

                                <div class="entry-content">
                                    <p>
                                        {{\Illuminate\Support\Str::limit(strip_tags($article->body),50)}}
                                    </p>
                                </div>
                            </div>
                            <a href="{{$article->urlslug()}}" class="btn">Read All Article</a>
                        </div>
                    </article>

                @empty
                    <h3>No Articles Added Yet</h3>

                    @endforelse





            </div><!-- col-md-8 /- -->
            <div class="col-md-1 col-sm-1"></div>
            <!-- col-md-3 -->
@include('_static.blogsidebar')
        </div>

    </div>
    @stop
