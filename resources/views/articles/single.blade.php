@extends('layouts.app')

@section('metas')
    <meta name="description" content="Top Cleaning Service in London">
    <title>{{$article->seo_title ?? $article->title}}</title>
    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{$article->image ? asset('uploads/'.$article->image) : asset('build/images/blog/blog-list-3.jpg')}}" />
@stop

@section('content')
    <div id="page-banner" class="page-banner">
        <img src="{{asset('build/images/page-banner.png')}}" alt="page-banner">
        <!-- container -->
        <div class="page-detail">
            <div class="container">
                <h1 class="page-title">{{$article->title}}</h1>
                <div class="page-breadcrumb pull-right">
                    <ol class="breadcrumb">
                        <li><a title="Home" href="/">Home</a></li>
                        <li><a href="{{route('blog.index')}}">Cleaning Articles</a></li>
                        <li>{{\Illuminate\Support\Str::limit($article->title,10)}}</li>
                    </ol>
                </div>
            </div>
        </div><!-- container /- -->
    </div>
    <div id="blog-section" class="blog-section blog-list ow-section">
        <!-- container -->
        <div class="container">
            <!-- col-md-8 -->
            <div class="col-md-8 col-sm-7 no-padding">
                <article>
                    <div class="blog-box">
                        <div class="entry-cover">
                            @if ($article->image)
                                <img src="{{asset('uploads/'.$article->image)}}"
                                     alt="{{$article->title}}">
                            @else
                                <img src="{{asset('build/images/blog/blog-list-3.jpg')}}"
                                     alt="{{$article->title}}">
                            @endif
                            <span class="posted-on">
								<span class="like">{{$article->views}}</span>
								<span class="date">{{$article->created_at->diffForHumans()}}</span>
							</span>
                        </div>
                        <div class="blog-box-inner">
                            <!-- entry header -->
                            <header class="entry-header">
                                <h1>{{$article->title}}</h1>
                            </header><!-- entry header /- -->

                            <footer class="entry-footer">

                                <span class="byline">
									<span class="screen-reader-text">Views </span>
									<a href="#">{{$article->views}}</a>
								</span>
                                <span class="byline">
									<span class="screen-reader-text">{{$article->maincategory->name}}</span>
								</span>
                            </footer>

                            <div class="entry-content">
                               {!! $article->body !!}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <aside class="widget widget_tags row">
                                <h5>Posted In Categories : </h5>
                                @forelse ($article->categories as $category)
                                    <a title="{{$category->name}}" href="{{$category->urlslug()}}">{{$category->name}}</a>
                                @empty
                                    <a title="{{$article->maincategory->name}}" href="{{$article->maincategory->urlslug()}}">{{$article->maincategory->name}}</a>

                                @endforelse

                            </aside>
                        </div>

                    </div>
                </article>

            </div><!-- col-md-8 /- -->
            <div class="col-md-1 col-sm-1"></div>
            <!-- col-md-3 -->
@include('_static.blogsidebar')
        </div><!-- container /- -->
    </div>

    @stop
