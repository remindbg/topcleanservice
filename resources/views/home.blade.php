@extends('layouts.app')


@section('metas')
    <meta name="description" content="Top Cleaning Service in London">
    <title>Domestic Cleaning Services Company in London</title>
    @stop

@section('content')

    <div id="slider-section" class="slider-section">
        <div id="make-clean-slider" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#make-clean-slider" data-slide-to="0" class="active"></li>

            </ol>

            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="{{asset('build/images/slider/slide-1.jpg')}}" alt="slide">
                    <div class="carousel-caption">
                        <div class="container">
                            <div class="col-md-8">
                                <h1>Domestic Cleaning Services in London</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <div class="container">
            <form id="quick-contact" class="contact-form col-md-4" method="get">
                <h3>Need Cleaning?<span>call us anytime !</span></h3>
                <div class="form-control">

                    <a href="https://www.cleandaylondon.com/get-a-cleaning-quote" target="_blank" class="submit btn btn-block btn-primary">Call US Now</a>
                </div>
                <div id="alert-msg" class="alert-msg"></div>
            </form>
        </div>
    </div><!-- Slider Section /- -->

    <!-- Service Section -->
    <section id="service-section" class="service-section ow-section">
        <!-- container -->
        <div class="container">

            <!-- Section Header -->
            <div class="section-header">
                <h3 class="text-primary">
                    <img src="{{asset('build/images/icon/sep-icon.png')}}" alt="services icon" />
                     Services We Offer  In London
                </h3>
            </div><!-- Section Header /- -->

            <div id="make-clean-service" class="owl-carousel owl-theme services-style1">
                <div class="item">
                    <div class="service-box">
                        <img src="{{asset('build/images/services/services-1.png')}}" alt="services"/>
                        <div class="service-box-inner">
                            <h4> Office & Commercial Cleaning </h4>
                            <a title="View Details" href="#">view details</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="service-box">
                        <img src="{{asset('build/images/services/services-2.png')}}" alt="services"/>
                        <div class="service-box-inner">
                            <h4> Windows & Upholstery Cleaning</h4>
                            <a title="View Details" href="#">view details</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="service-box">
                        <img src="{{asset('build/images/services/services-3.png')}}" alt="services"/>
                        <div class="service-box-inner">
                            <h4>House cleaning &amp; Maid Service</h4>
                            <a title="View Details" href="#">view details</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="service-box">
                        <img src="{{asset('build/images/services/services-1.png')}}" alt="services"/>
                        <div class="service-box-inner">
                            <h4> Office & Commercial Cleaning </h4>
                            <a title="View Details" href="#">view details</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="service-box">
                        <img src="{{asset('build/images/services/services-2.png')}}" alt="services"/>
                        <div class="service-box-inner">
                            <h4> Windows & Upholstery Cleaning</h4>
                            <a title="View Details" href="#">view details</a>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="service-box">
                        <img src="{{asset('build/images/services/services-3.png')}}" alt="services"/>
                        <div class="service-box-inner">
                            <h4>House cleaning &amp; Maid Service</h4>
                            <a title="View Details" href="#">view details</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container /- -->
    </section><!-- Service Section /- -->

    <!-- Welcome Section -->
    <Section id="welcome-section" class="welcome-section ow-section">
        <!-- container -->
        <div class="container">
            <div class="col-md-4 col-sm-5">
                <img src="{{asset('build/images/welcome/welcome-img.png')}}" alt="welcome" />
            </div>
            <div class="col-md-8 col-sm-7 welcome-content">
                <!-- Section Header -->
                <div class="section-header">
                    <h3>About Our Services</h3>
                </div><!-- Section Header /- -->
                <p>
                    We offer professional cleaning services in all <strong>london</strong> areas. We specialize in home / domestic cleaning.
                </p>
                <a title="Button" href="/london-services">Learn More</a>
                <div class="welcome-content-box row">
                    <div class="col-md-4 col-sm-6 welcome-box">
                        <img src="{{asset('build/images/welcome/high-trained-staff.png')}}" alt="high trained staff" />
                        <h4>HIGHLY-TRAINED STAFF</h4>
                    </div>
                    <div class="col-md-4 col-sm-6 welcome-box">
                        <img src="{{asset('build/images/welcome/quality-cleaning-staff.png')}}" alt="quality-cleaning-staff" />
                        <h4>Professional  Tools</h4>
                    </div>
                    <div class="col-md-4 col-sm-6 welcome-box">
                        <img src="{{asset('build/images/welcome/fast-service.png')}}" alt="fast-service" />
                        <h4>Reliable Prices on Time</h4>
                    </div>
                </div>
            </div>
        </div><!-- container /- -->
    </section><!-- Welcome Section /- -->

    <!-- Team Section -->
    <section id="team-section" class="team-section ow-section">
        <!-- container -->
        <div class="container">
            <!-- col-md-3 -->

                <!-- Section Header -->
                <div class="section-header">
                    <h3><img src="{{asset('build/images/icon/sep-icon.png')}}" alt="sep-icon" />Popular Products from Our Shop</h3>
                </div><!-- Section Header /- -->
                <p>Check some of our  products from the shop page</p>


            <!-- col-md-9 -->
            <div class="col-md-12 col-sm-8">
                <ul class="product-main">
                    <li class="col-md-4 col-sm-4 col-xs-6">
                        <div class="product-box">
                            <a title="Product" href="#">
                                <img src="{{asset('build/images/product/product-8.jpg')}}" alt="product">
                                <div class="product-hover">

                                    <ul>
                                        <li>from 3.55</li>
                                    </ul>
                                </div>
                            </a>
                            <div class="product-detail">
                                <a title="Product Name" href="#" class="product-title">Microfiber Magic Sponge - 3 Piece Pack </a>
                                <span>
										<span class="price">from 10.50</span>
										<a title="View" href="#" class="btn pull-right">Add to Cart</a>
									</span>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 col-sm-4 col-xs-6">
                        <div class="product-box">
                            <a title="Product" href="#">
                                <img src="{{asset('build/images/product/product-8.jpg')}}" alt="product">
                                <div class="product-hover">

                                    <ul>
                                        <li>from 3.55</li>
                                    </ul>
                                </div>
                            </a>
                            <div class="product-detail">
                                <a title="Product Name" href="#" class="product-title">Microfiber Magic Sponge - 3 Piece Pack </a>
                                <span>
										<span class="price">from 10.50</span>
										<a title="View" href="#" class="btn pull-right">Add to Cart</a>
									</span>
                            </div>
                        </div>
                    </li>
                    <li class="col-md-4 col-sm-4 col-xs-6">
                        <div class="product-box">
                            <a title="Product" href="#">
                                <img src="{{asset('build/images/product/product-8.jpg')}}" alt="product">
                                <div class="product-hover">

                                    <ul>
                                        <li>from 3.55</li>
                                    </ul>
                                </div>
                            </a>
                            <div class="product-detail">
                                <a title="Product Name" href="#" class="product-title">Microfiber Magic Sponge - 3 Piece Pack </a>
                                <span>
										<span class="price">from 10.50</span>
										<a title="View" href="#" class="btn pull-right">Add to Cart</a>
									</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div><!-- container /- -->
    </section><!-- Team Section /- -->

    <!-- Industry Serve -->
    <section id="industry-serve-section" class="industry-serve-section ow-section">
        <!-- container -->
        <div class="container">
            <!-- col-md-6 -->
            <div class="col-md-12">
                <!-- Section Header -->
                <div class="section-header">
                    <h3><img src="{{asset('build/images/icon/sep-icon.png')}}" alt="sep-icon" /> INDUSTRIES WE SERVE</h3>
                </div><!-- Section Header /- -->
                <!-- industry-serve -->
                <div class="industry-serve">
                    <p>We can cover not only home / domestic cleaning, but commercial cleaning services</p>
                    <div class="row">
                        <p class="col-md-6 col-sm-6"><img src="{{asset('build/images/icon/airline.png')}}" alt="airline" /> Airports & Airlines</p>
                        <p class="col-md-6 col-sm-6"><img src="{{asset('build/images/icon/school.png')}}" alt="school" /> Schools & Universities</p>
                        <p class="col-md-6 col-sm-6"><img src="{{asset('build/images/icon/auto-mobile.png')}}" alt="auto-mobile" /> Auto Dealerships</p>
                        <p class="col-md-6 col-sm-6"><img src="{{asset('build/images/icon/medical.png')}}" alt="medical" /> Medical Facilities</p>
                        <p class="col-md-6 col-sm-6"><img src="{{asset('build/images/icon/fitness.png')}}" alt="fitness" /> Sports & Fitness Centers</p>
                        <p class="col-md-6 col-sm-6"><img src="{{asset('build/images/icon/entertainment.png')}}" alt="airline" /> Entertainment Venues</p>
                        <a title="View More" href="#">View More</a>
                    </div>
                </div><!-- industry-serve -->
            </div>



        </div><!-- container /- -->
    </section><!-- Industry Serve /- -->

    <!-- Statistics Section -->
    <div id="statistics-section" class="statistics-section ow-background">
        <!-- container -->
        <div class="container">
            <!-- col-md-3 -->
            <div class="col-md-6 col-sm-3 col-xs-6">
                <div class="statistics-box">
                    <h3>
                        <img src="{{asset('build/images/statistics/satisfied-customer.png')}}" alt="satisfied-customer" />
                        <span class="count" id="statistics_1_count-1" data-statistics_percent="3540"></span>
                    </h3>
                    <img src="{{asset('build/images/icon/brush.png')}}" alt="brush" />
                    <h4>satisfied customers</h4>
                </div>
            </div><!-- col-md-3 /- -->
            <!-- col-md-3 -->
            <div class="col-md-6 col-sm-3 col-xs-6">
                <div class="statistics-box">
                    <h3>
                        <img src="{{asset('build/images/statistics/building.png')}}" alt="building" />
                        <span class="count" id="statistics_1_count-2" data-statistics_percent="1237"></span>
                    </h3>
                    <img src="{{asset('build/images/icon/brush.png')}}" alt="brush" />
                    <h4>buldings cleaned</h4>
                </div>
            </div>


        </div><!-- container /- -->
    </div><!-- Statistics Section /- -->

    <!-- Blog Section -->
    <div id="blog-section" class="blog-section ow-section">
        <!-- container -->
        <div class="container">
            <!-- Section Header -->
            <div class="section-header">
                <h3><img src="{{asset('build/images/icon/sep-icon.png')}}" alt="sep-icon" />LATEST NEWS AND TIPS</h3>
            </div><!-- Section Header /- -->
            <!-- col-md-6 -->
            @forelse($articles as $article)
            <article class="col-md-4 col-sm-12">
                <div class="123">
                    <div class="3333">
                        @if ($article->image)
                            <img src="{{asset('uploads/'.$article->image)}}"
                                 alt="{{$article->title}}">
                        @else
                            <img src="{{asset('build/images/blog/blog-list-3.jpg')}}"
                                 alt="{{$article->title}}">
                    @endif
                        <!-- entry header -->
                        <header class="entry-header">
                            <h3><a title="{{$article->title}}" class="text-primary" href="#">{{$article->title}}</a></h3>
                        </header><!-- entry header /- -->

                        <footer class="entry-footer">
							<span class="byline">
								<span class="screen-reader-text"> </span>
								<a title="category" href="{{$article->maincategory->urlslug()}}">{{$article->maincategory->name}}</a>
							</span>
                            <span class="byline">
								<span class="screen-reader-text">Views </span>

								<a title="Likes" href="#">{{$article->views}}</a>
							</span>
                        </footer>

                        <div class="entry-content">
                            <p>{!! \Illuminate\Support\Str::limit(strip_tags($article->body),120,'[...]') !!}</p>
                        </div>
                        <a href="#">Read more</a>
                    </div>

                </div>
            </article><!-- col-md-6 /- -->
            @empty
                @endforelse
            <!-- col-md-6 -->

            <a href="/cleaning-articles" class="btn">View All</a>
        </div><!-- container /- -->
    </div><!-- Blog Section /- -->

    <!-- call out Section -->
    <section id="call-out-section" class="call-out-section">
        <!-- container -->
        <div class="container">
            <!-- Section-header -->
            <div class="section-header">
                <h3>clean and uniqe psd for your website!</h3>
            </div><!-- Section-header /- -->
            <p>Quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores ratione voluptatem
                Neque porro quisquam est, qui dolorem ipsum quia dolor amet.</p>
            <a title="Button" href="#" class="btn">Buy Now</a>
            <a title="Button" href="#" class="btn">Contact Us</a>
        </div><!-- container -->
    </section><!-- call out section /- -->

@stop

