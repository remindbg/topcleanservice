<!doctype html>

<head>
   @include('_static.header')

</head>
<body data-offset="200" data-spy="scroll" data-target=".primary-navigation">
<a id="top"></a>

<!-- Header Section -->
<div class="row">
    <div class="col-lg-12">
        @include('rpanel._components.messages')

    </div>
</div>

@include('_static.topmenu')

<!-- Slider Section -->
@yield('content')
<!-- Footer Section -->
<footer id="footer-section" class="footer-section ow-background">
    <!-- container -->
    <div class="container">
        <div class="footer-heading">
            <h5>Have any questions about our cleaning services?</h5>
            <h3>Call Now: +(01) 800 527 4800</h3>
        </div>
        <aside class="col-md-3 col-sm-3 widget widget-link">
            <h3>Clenaing services</h3>
            <ul>
                <li><a title="Home Cleaning" href="#"><i class="fa fa-tint"></i> Home Cleaning</a></li>
                <li><a title="Building Cleaning" href="#"><i class="fa fa-tint"></i> Building Cleaning</a></li>
                <li><a title="Commercial Cleaning" href="#"><i class="fa fa-tint"></i> Commercial Cleaning</a></li>
                <li><a title="Vehicle Cleaning" href="#"><i class="fa fa-tint"></i> Vehicle Cleaning</a></li>
                <li><a title="Maintenance Service" href="#"><i class="fa fa-tint"></i> Maintenance Service</a></li>
                <li><a title="Pest Controls" href="#"><i class="fa fa-tint"></i> Pest Control</a></li>
            </ul>
        </aside>

        <aside class="col-md-3 col-sm-3 widget widget-link">
            <h3>help</h3>
            <ul>
                <li><a title="Contact Us" href="#"><i class="fa fa-tint"></i> Contact Us</a></li>
                <li><a title="Special Services" href="#"><i class="fa fa-tint"></i> Special Services</a></li>
                <li><a title="Advice Tips" href="#"><i class="fa fa-tint"></i> Advice & Tips</a></li>
                <li><a title="News Video" href="#"><i class="fa fa-tint"></i> News & Video</a></li>
                <li><a title="Terms Condition" href="#"><i class="fa fa-tint"></i> Terms & Condition</a></li>
                <li><a title="Site Map" href="#"><i class="fa fa-tint"></i> Site Map</a></li>
            </ul>
        </aside>

        <aside class="col-md-2 col-sm-2 widget widget-link">
            <h3>page links</h3>
            <ul>
                <li><a title="Home" href="#"><i class="fa fa-tint"></i> Home</a></li>
                <li><a title="About Us" href="#"><i class="fa fa-tint"></i> About Us</a></li>
                <li><a title="Shop" href="#"><i class="fa fa-tint"></i> Shop</a></li>
                <li><a title="Our Staffs" href="#"><i class="fa fa-tint"></i> Our Staffs</a></li>
                <li><a title="Services" href="#"><i class="fa fa-tint"></i> Services</a></li>
                <li><a title="Latest News" href="#"><i class="fa fa-tint"></i> Latest News</a></li>
            </ul>
        </aside>

        <aside class="col-md-4 col-sm-4 widget widget-calculator">
            <h3>Price Calculator</h3>
            <form>
                <input type="text" name="square-meter" placeholder="SIZE OF AREA  (sQUARE METERS)" required />
                <select>
                    <option>choose the type of service</option>
                    <option>choose the type of service</option>
                    <option>choose the type of service</option>
                    <option>choose the type of service</option>
                </select>
                <input type="submit" value="submit query" />
            </form>
        </aside>

        <!-- Footer Bottom -->
        <div class="footer-bottom">
            <p>Copyright © 2015 Make Clean. All Rights Reserved.</p>
        </div>
        <!-- Footer Bottom /- -->

    </div>
    <!-- container /- -->
</footer><!-- Footer Section /- -->

<!-- jQuery Include -->
<script src="{{asset('build/libraries/jquery.min.js')}}"></script>
<script src="{{asset('build/libraries/jquery.easing.min.js')}}"></script><!-- Easing Animation Effect -->
<script src="{{asset('build/libraries/bootstrap/bootstrap.min.js')}}"></script> <!-- Core Bootstrap v3.3.4 -->
<script src="{{asset('build/libraries/gmap/jquery.gmap.min.js')}}"></script> <!-- Light Box -->
<script src="{{asset('build/libraries/fuelux/jquery-ui.min.js')}}"></script> <!-- Price Filter -->
<script src="{{asset('build/libraries/jquery.animateNumber.min.js')}}"></script> <!-- Used for Animated Numbers -->
<script src="{{asset('build/libraries/jquery.appear.js')}}"></script> <!-- It Loads jQuery when element is appears -->
<script src="{{asset('build/libraries/wow.min.js')}}"></script> <!-- Use For Animation -->
<script src="{{asset('build/libraries/owl-carousel/owl.carousel.min.js')}}"></script> <!-- Core Owl Carousel CSS File  *	v1.3.3 -->
<script src="{{asset('build/libraries/portfolio-filter/jquery.quicksand.js')}}"></script> <!-- Portfolio Filter -->
<script src="{{asset('build/libraries/expanding-search/modernizr.custom.js')}}"></script> <!-- Core Owl Carousel CSS File  *	v1.3.3 -->
<script src="{{asset('build/libraries/flexslider/jquery.flexslider-min.js')}}"></script> <!-- flexslider -->
<script src="{{asset('build/libraries/jquery.magnific-popup.min.js')}}"></script> <!-- Light Box -->
<script src="{{asset('build/libraries/expanding-search/modernizr.custom.js')}}"></script> <!-- Core Owl Carousel CSS File  *	v1.3.3 -->
<script src="{{asset('build/libraries/expanding-search/classie.js')}}"></script>

<!-- Customized Scripts -->
<script src="{{asset('build/js/functions.js')}}"></script>

</body>
</html>
