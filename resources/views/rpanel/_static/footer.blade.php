
<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>ReminD</b>
    </div>
    <strong>Copyright &copy; 2020</strong>
</footer>

</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('rpanel/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('rpanel/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- FastClick -->
<!-- AdminLTE App -->
<script src="{{asset('rpanel/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('rpanel/dist/js/demo.js')}}"></script>

@yield('scripts')

</body>
</html>
