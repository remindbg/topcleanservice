<!-- Brand Logo -->

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
        </div>
        <div class="info">
            <a href="#" class="d-block">Admin</a>
        </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->

            <li class="nav-item">
                <a href="{{route('rpanel.welcome')}}" class="nav-link">
                    <i class="nav-icon fas fa-home"></i>
                    <p>
                        Home Admin
                    </p>
                </a>
            </li>
            <li class="nav-header">Blog Section</li>


            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon far fa-paper-plane"></i>
                    <p>
                        Articles
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('rpanel.article.index')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Articles</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('rpanel.article.create')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>New Article</p>
                        </a>
                    </li>

                </ul>
            </li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        Categories
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('rpanel.category.index')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Categories</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('rpanel.category.create')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>New Category</p>
                        </a>
                    </li>

                </ul>
            </li>


            <li class="nav-header">Services</li>
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>
                        Services
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{route('rpanel.services.index')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Services</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('rpanel.services.create')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>New Service </p>
                        </a>
                    </li>

                </ul>
            </li>



            <li class="nav-header">QuickContact</li>

            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon far fa-paper-plane"></i>
                    <p>
                        All Contacts
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All Contacts</p>
                        </a>
                    </li>


                </ul>
            </li>

            <li class="nav-header">Shop</li>

            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fa fa-cart-plus"></i>
                    <p>
                        All Products
                        <i class="fas fa-angle-left right"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="" class="nav-link">
                            <i class="fa fa-cart-plus nav-icon"></i>
                            <p>New Product</p>
                        </a>
                    </li>


                </ul>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
