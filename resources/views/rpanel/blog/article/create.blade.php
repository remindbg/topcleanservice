
@extends('rpanel.layouts.app')

@section('title','New Article')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">New Article</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{route('rpanel.article.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-7">
                            @method('post')

                            <div class="card card-success">
                                <div class="card-header">
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                        <button type="button" class="btn btn-tool"
                                                data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                    <h3 class="card-title">General</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <small>Title</small>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" value=""
                                                       name="title">

                                            </div>
                                        </div>


                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Main Category</label>
                                                        <select class="form-control" name="main_category_id">
                                                            @forelse($categories as $category)
                                                                @if ($category->parent_id == 0)
                                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                                @else
                                                                    <option value="{{$category->id}}"
                                                                            class="mr-2">---{{$category->name}}</option>
                                                                @endif
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label> Body<span
                                                                class="small"></span></label>
                                                        <textarea class="form-control" rows="22" cols="10" name="body"
                                                                  id="body"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="form-group">
                                                        <label>Is Active?</label>
                                                        <select class="form-control" name="is_active">
                                                            <option value="0">No</option>
                                                            <option value="1" selected>Yes</option>

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-lg-10">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile">Image</label>
                                                        <div class="input-group">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input"
                                                                       name="feautured_image"
                                                                       id="exampleInputFile">
                                                                <label class="custom-file-label"
                                                                       for="exampleInputFile">File</label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Категории</label> <span class="small">*Да се впише и
                                            главната тук - задължително!!!</span>
                                                        <select class="select2" name="categories[]" multiple="multiple"
                                                                data-placeholder="категории"
                                                                style="width: 100%;">
                                                            @foreach ($categories as $category)
                                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>


                                </div>
                                <hr>
                                <button type="submit" class="btn btn-block btn-info btn-lg">Create</button>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="card card-blue collapsed-card">
                                <div class="card-header ">
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-plus"></i></button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                    <h3 class="card-title">SEO</h3>
                                </div>
                                <div class="card-body">
                                    <div class="input-group mb-3">

                                    </div>

                                    <div class="form-group">
                                        <label>SEO Title</label>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="" name="seo_title"
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label>SEO Description</label>
                                        <textarea class="form-control" rows="3"
                                                  name="seo_description"></textarea>
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label for="exampleInputFile">SEO Image ( OG Image )</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input"
                                                           name="og_image"
                                                           id="exampleInputFile">
                                                    <label class="custom-file-label"
                                                           for="exampleInputFile">File</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
@stop

@section('scripts')
    <script src="{{asset('rpanel/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            });
            $('#tags').select2({
                theme: 'bootstrap4',
                tags: true,
                // data: ["Clare","Cork","South Dublin"],
                tokenSeparators: [','],

                /* the next 2 lines make sure the user can click away after typing and not lose the new tag */

            });
        });



    </script>
    <script src="{{asset('rpanel/tinymce/tinymce.min.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 500,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount codesample', 'code image'
            ],
            toolbar: 'image code | codesample | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'],
            image_title: true,
            automatic_uploads: true,
            images_upload_url: '{{route('rpanel.uploadimage')}}',
            file_picker_types: 'image',
            document_base_url : "/",
            relative_urls : false,
            remove_script_host : false,
            images_upload_base_path: '',
            images_upload_credentials: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                        console.log(blobInfo);
                    };
                };


            }
        });
        let img = tinymce.selection.getNode();


    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('rpanel/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('rpanel/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>
        .select2-results__option[aria-selected=true] {
            display: none;
        }
    </style>

@endsection


