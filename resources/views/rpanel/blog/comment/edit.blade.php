
@extends('rpanel.layouts.app')

@section('title','New Category')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Edit Category</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{route('rpanel.category.update',$category->id)}}" method="POST"
                      enctype="multipart/form-data">
                    @csrf

                    <div class="row">
                        <div class="col-lg-7">
                            @method('PUT')

                            <div class="card card-success">
                                <div class="card-header">
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                        <button type="button" class="btn btn-tool"
                                                data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                    <h3 class="card-title">General</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <small>Name</small>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" value="{{$category->name}}"
                                                       name="name">

                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <p>SLUG</p>
                                            <div class="input-group mb-3">


                                                <input type="text" class="form-control" value="{{$category->slug}}"
                                                       name="slug">

                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label>Main Category</label>
                                                        <select class="form-control" name="parent_id">
                                                            <option value="0" selected>No</option>
                                                            @forelse($allcats as $cat)
                                                                @if($cat->id == $category->id)
                                                                    @break
                                                                @endif
                                                                <option value="{{$cat->id}}" {{$category->id ==
                                                                $category->parent_id ? 'selected' : ''}}
                                                                >{{$cat->name}}</option>
                                                            @empty
                                                            @endforelse
                                                        </select>
                                                    </div>

                                                </div>

                                                <div class="col-lg-10">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile">Image</label>
                                                        <div class="input-group">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input"
                                                                       name="cat_image"
                                                                       id="exampleInputFile">
                                                                <label class="custom-file-label"
                                                                       for="exampleInputFile">File</label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label> Description</label>
                                                        <textarea class="form-control"
                                                                  rows="3" name="description"
                                                                  id="body">{{$category->description}}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <hr>
                                <button type="submit" class="btn btn-block btn-info btn-lg">Update</button>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="card card-blue collapsed-card">
                                <div class="card-header ">
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-plus"></i></button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                    <h3 class="card-title">SEO</h3>
                                </div>
                                <div class="card-body">
                                    <div class="input-group mb-3">

                                    </div>

                                    <div class="form-group">
                                        <label>SEO Title</label>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$category->seo_title}}"
                                               name="seo_title"
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label>SEO Description</label>
                                        <textarea class="form-control" rows="3"
                                                  name="seo_description">{{$category->seo_description}}</textarea>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
@stop

