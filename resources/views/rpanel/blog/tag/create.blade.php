
@extends('rpanel.layouts.app')

@section('title','New Tag')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">New Tag</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{route('rpanel.tag.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-5">
                            @method('post')

                            <div class="card card-success">
                                <div class="card-header">
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                        <button type="button" class="btn btn-tool"
                                                data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                    <h3 class="card-title">General</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <small>Name</small>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" value=""
                                                       name="name">

                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <p>SLUG</p>
                                            <div class="input-group mb-3">


                                                <input type="text" class="form-control" value=""
                                                       name="slug">

                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <hr>
                                <button type="submit" class="btn btn-block btn-info btn-lg">Create</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
@stop

