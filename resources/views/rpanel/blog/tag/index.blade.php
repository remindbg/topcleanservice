
@extends('rpanel.layouts.app')

@section('title','New Tag')

@section('content')
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">All Tags</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">

                <a href="{{route('rpanel.tag.create')}}">
                    <button type="button" class="btn btn-info btn-sm">Create</button>
                </a>
                <hr>
                <table class="table table-condensed">
                    <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Name</th>
                        <th>Slug</th>
                        <th>Assigned Articles</th>
                        <th>Action</th>

                    </tr>
                    </thead>
                    <tbody>
                    @forelse($tags as $tag)
                    <tr>
                        <td>{{$tag->id}}</td>
                        <td>{{$tag->name}}</td>
                        <td>{{$tag->slug}}</td>
                        <td>{{$tag->articles->count()}}</td>
                        <td class="project-actions">
                            <a class="btn btn-primary btn-sm" href="#" target="_blank">
                                <i class="fas fa-folder"></i>View

                            </a>
                            <a class="btn btn-info btn-sm" href="{{route('rpanel.tag.edit',$tag->id)}}">
                                <i class="fas fa-pencil-alt"></i>Edit
                            </a>
                            <a class="btn btn-danger btn-sm" href="" data-toggle="modal"
                               data-target="#modal-danger-{{$tag->id}}">
                                <i class="fas fa-trash"></i>Delete
                            </a>
                        </td>
                    </tr>
                    <div class="modal fade" id="modal-danger-{{$tag->id}}">
                        <div class="modal-dialog">
                            <div class="modal-content bg-danger">
                                <div class="modal-header">
                                    <h4 class="modal-title">Delete?</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>

                                <div class="modal-footer justify-content-between">
                                    <form action="{{route('rpanel.tag.destroy',$tag->id)}}"
                                          method="POST">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    <button type="button" class="btn btn-outline-light"
                                            data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>

                    @empty

                    @endforelse

                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
@stop

