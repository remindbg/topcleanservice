@extends('admin.layouts.app')

@section('title','New Facts')

@section('content')

    <h5 class="mb-2">New Fact</h5>

    <div class="card-body">
        <form action="{{route('admin.facts.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    @method('post')


                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Обща Информация</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <small>Заглавие</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value=""
                                               name="title">

                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <p>SLUG - генерира се автоматично от тайтъла -   после може да се редактира </p>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value=""
                                               name="slug" disabled>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Категория / задължително е да има категория заради
                                                    slug-a който depend-ва на категориите!</label>
                                                <select class="form-control" name="category_id">
                                                    @forelse($categories as $category)
                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Текст<span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="22" cols="10" name="body"
                                                          id="body"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Категории</label> <span class="small">*Да се впише и
                                            главната тук - задължително!!!</span>
                                        <select class="select2" name="categories[]" multiple="multiple"
                                                data-placeholder="категории"
                                                style="width: 100%;">
                                            @foreach ($categories as $category)
                                                <option value="{{$category->id}}">{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Добавяне на Факт</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            });
            $('#tags').select2({
                theme: 'bootstrap4',
                tags: true,
                // data: ["Clare","Cork","South Dublin"],
                tokenSeparators: [','],

                /* the next 2 lines make sure the user can click away after typing and not lose the new tag */

            });
        });



    </script>
    <script src="{{asset('admin/tinymce/tinymce.min.js')}}"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 500,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount codesample', 'code image'
            ],
            toolbar: 'image code | codesample | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'],
            image_title: true,
            automatic_uploads: true,
            {{--images_upload_url: '{{url('panel/articles/uploadimage')}}',--}}
            file_picker_types: 'image',
            document_base_url : "/",
            relative_urls : false,
            remove_script_host : false,
            images_upload_base_path: '/',
            images_upload_credentials: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                        console.log(blobInfo);
                    };
                };


            }
        });
        let img = tinymce.selection.getNode();


    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>
        .select2-results__option[aria-selected=true] {
            display: none;
        }
    </style>

@endsection
