@extends('admin.layouts.app')


@section('content')

    <h5 class="mb-2">Edit a Comment</h5>

    <div class="card-body">
        <form action="{{route('admin.comment.update',$comment->id)}}" method="POST" >
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">General Information</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Task Author</label>
                                                <select class="form-control" name="creator_id">

                                                        <option value="{{$comment->creator->id}}" disabled selected>
                                                            {{$comment->creator->name}}
                                                          </option>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Text<span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="5" cols="5" name="text"
                                                          id="body">{{$comment->text}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>


                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Edit Comment</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')


@endsection


@section('css')


@endsection
