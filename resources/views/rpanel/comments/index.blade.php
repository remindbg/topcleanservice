@extends('admin.layouts.app')
@section('title','Comments')

@section('content')
    <div class="card-body table-responsive p-0">

        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>body....</th>

                <th> Task</th>
                <th>Author</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
            @forelse($comments as $comment)
                <tr class="">
                    <td>{{$comment->id}}</td>
                    <td>{{\Illuminate\Support\Str::limit($comment->text,15)}}</td>


                    <td>
                        <a href="{{route('task.show',$comment->task->id)}}">
                            {{\Illuminate\Support\Str::limit($comment->task->name,22)}}
                        </a>
                    </td>
                    <td><a href="{{route('user.show',$comment->creator->id)}}">{{$comment->creator->name}}</a></td>


                    <td>{{$comment->created_at}}</td>


                    <td>
                        <a href="{{route('admin.comment.edit', $comment->id)}}"><button class="btn
                        btn-primary
                        btn-sm">Edit</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                data-target="#modal-danger-{{$comment->id}}">
                            Delete
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$comment->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete Comment?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.comment.destroy',$comment->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
               No Comments Yet
            @endforelse
            </tbody>
        </table>
        {{$comments->links()}}
    </div>
    <!-- /.modal -->
@stop
