@extends('admin.layouts.app')

@section('title','New Priority')

@section('content')

    <h5 class="mb-2">New Priority Label</h5>

    <div class="card-body">
        <form action="{{route('admin.priority.store')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    @method('post')

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">General Information</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <small>Priority Name</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value=""
                                               name="status">

                                    </div>
                                </div>





                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    <p>Priority Label?</p>
                                    <div class="form-group">

                                        <select class="form-control" name="label">
                                            <option value="bg-success" class="bg-success">Success</option>
                                            <option value="bg-warning" class="bg-warning">Warning</option>
                                            <option value="bg-danger" class="bg-danger">Danger</option>
                                            <option value="bg-info" class="bg-info">Info</option>

                                            <option value="0" selected>No Label</option>

                                        </select>
                                    </div>

                                </div>





                            </div>
                        </div>


                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Create New Priority</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')


@endsection


@section('css')


@endsection
