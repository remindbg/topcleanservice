@extends('admin.layouts.app')

@section('title','Редакция на Факт')

@section('content')

    <h5 class="mb-2">Редакция на Факт: {{$fact->title}}</h5>

    <div class="card-body">
        <form action="{{route('admin.facts.update',$fact->id)}}" method="POST" enctype="multipart/form-data">

            <div class="row">
                <div class="col-lg-7">
                    @method('put')
                    @csrf

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">Обща Информация</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <small>Заглавие</small>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$fact->title}}"
                                               name="title">

                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <p>SLUG</p>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$fact->slug}}"
                                               name="slug" >
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <p>Активна?</p>
                                    <div class="form-group">

                                        <select class="form-control" name="is_active">

                                            <option value="1" {{$fact->is_active ? 'selected'
                                                : ''}}>Активен</option>
                                            <option value="0" {{$fact->is_active ? '' :
                                                'selected'}}>Неактивен</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Главна Категория</label>
                                                <select class="form-control" name="category_id">
                                                    @forelse($categories as $category)
                                                        @if ($category->parent_id == 0)
                                                            <option value="{{$category->id}}" {{$fact->maincategory->id == $category->id ?
                                                                'selected' : ''
                                                                }}>{{$category->name}}</option>
                                                        @else
                                                            <option value="{{$category->id}}"
                                                                    class="mr-2"
                                                                {{$fact->maincategory->id == $category->id ?
                                                                'selected' : ''
                                                                }}>---{{$category->name}}</option>
                                                        @endif
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Текст <span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="7" name="body"
                                                          id="body">{!! $fact->body !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Категории</label> <span class="small">*Да се впише и
                                            главната тук - задължително!!!</span>
                                        <select class="select2" name="categories[]" multiple="multiple"
                                                data-placeholder="категории"
                                                style="width: 100%;">
                                            @foreach ($categories as $category)
                                                <option value="{{$category->id}}"
                                                    {{$fact->categories->contains($category->id) ? 'selected' :
                                                    ''}}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Редакция</button>
                    </div>
                </div>
                <div class="col-lg-5">
                    <div class="card card-success collapsed-card">
                        <div class="card-header ">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-plus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">SEO Информация</h3>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label>SEO Заглавие</label>
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" value="{{$fact->seo_title}}" name="seo_title">
                            </div>

                            <div class="form-group">
                                <label>SEO Описание</label>
                                <textarea class="form-control" rows="3" placeholder="около 120 символа"
                                          name="seo_description">{!!$fact->seo_description  !!}</textarea>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </form>

    </div>

@stop



@section('scripts')
    <script src="{{asset('admin/plugins/select2/js/select2.full.min.js')}}"></script>
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2({
                theme: 'bootstrap4'
            });
            $('#tags').select2({
                theme: 'bootstrap4',
                tags: true,

                // data: [],
                tokenSeparators: [','],

                /* the next 2 lines make sure the user can click away after typing and not lose the new tag */

            });
        });



    </script>
    <script src="{{asset('admin/tinymce/tinymce.min.js')}}"></script>
    <script>
        tinymce.init({
            selector: '#body',
            height: 500,
            menubar: true,
            plugins: [
                'advlist  lists link image  print preview anchor ',
                'searchreplace visualblocks code fullscreen',
                ' media table contextmenu paste code help wordcount ', 'code image'
            ],
            toolbar: 'image code | codesample | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',

            image_title: true,
            automatic_uploads: true,
            images_upload_url: '{{url('panel/article/uploadimage')}}', //todo
            file_picker_types: 'image',
            document_base_url : "/",
            relative_urls : false,
            remove_script_host : false,
            images_upload_base_path: '/',
            images_upload_credentials: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                input.onchange = function() {
                    var file = this.files[0];
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                        var id = 'blobid' + (new Date()).getTime();
                        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                        var base64 = reader.result.split(',')[1];
                        var blobInfo = blobCache.create(id, file, base64);
                        blobCache.add(blobInfo);
                        cb(blobInfo.blobUri(), { title: file.name });
                        console.log(blobInfo);
                    };
                };


            }
        });



    </script>

@endsection


@section('css')
    <link rel="stylesheet" href="{{asset('admin/plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <style>
        .select2-results__option[aria-selected=true] {
            display: none;
        }
    </style>

@endsection
