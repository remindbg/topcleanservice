@extends('admin.layouts.app')
@section('title','Priorities')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.priority.create')}}">
            <button type="button" class="btn btn-info btn-sm">Add Priority</button>
        </a>
        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Name</th>

                <th> Label</th>
                <th>Assigned Tasks</th>

                <th>Date</th>
                <th>Action</th>
            </tr>
            @forelse($priorities as $priority)
                <tr class="">
                    <td>{{$priority->id}}</td>
                    <td>{{\Illuminate\Support\Str::limit($priority->status,22)}}</td>

                    <td class="">
                        <button disabled class=" btn {{$priority->label}}"></button></td>
                    <td>
                        {{$priority->tasks->count()}}
                    </td>

                    <td>{{$priority->created_at}}</td>


                    <td>
                        <a href="{{route('admin.priority.edit', $priority->id)}}"><button class="btn
                        btn-primary
                        btn-sm">Edit</button></a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                data-target="#modal-danger-{{$priority->id}}">
                            Delete
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$priority->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete Priority?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.priority.destroy',$priority->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
               No Priorities Yet
            @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.modal -->
@stop
