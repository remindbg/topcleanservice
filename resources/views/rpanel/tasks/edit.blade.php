@extends('admin.layouts.app')


@section('content')

    <h5 class="mb-2">Edit: {{$task->name}}</h5>

    <div class="card-body">
        <form action="{{route('admin.task.update',$task->id)}}" method="POST" >
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">General Information</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label> Task Name</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$task->name}}"
                                               name="name">

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Task Author</label>
                                                <select class="form-control" name="creator_id">
                                                    @forelse($users as $user)
                                                        <option value="{{$user->id}}"
                                                            {{$task->creator->id ==
                                                            $user->id ? 'selected' : ''}}>{{$user->name}}</option>
                                                    @empty
                                                    @endforelse
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label> Body<span
                                                        class="small"></span></label>
                                                <textarea class="form-control" rows="22" cols="10" name="body"
                                                          id="body">{{$task->body}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Priority</label>
                                        <select class="form-control" name="priority_id">
                                            @forelse($priorities as $priority)
                                                <option value="{{$priority->id}}"
                                                    {{$task->priority->id ==
                                                    $priority->id ? 'selected' : ''}}>{{$priority->status}}</option>
                                            @empty
                                            @endforelse
                                        </select>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Task Status</label>
                                            <select class="form-control" name="is_finished">

                                                <option value="1" {{$task->is_finished ? 'selected' :
                                                ''}}>Closed</option>
                                                <option value="0" {{$task->is_finished ? '' : 'selected'}}>Open
                                                </option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Edit Task</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')

    <script src="{{asset('admin/tinymce/tinymce.min.js')}}"></script>

    <script>
        tinymce.init({
            selector: 'textarea',
            height: 500,
            menubar: true,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount codesample', 'code image'
            ],
            toolbar: 'image code | codesample | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'],




        });
        let img = tinymce.selection.getNode();


    </script>

@endsection


@section('css')


@endsection
