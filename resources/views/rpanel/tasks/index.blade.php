@extends('admin.layouts.app')

@section('content')
    <div class="card-body table-responsive p-0">
        <a href="{{route('admin.task.create')}}">
            <button type="button" class="btn btn-info btn-sm">Add Task</button>
        </a>
        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>name</th>
                <th>Priority</th>

                <th>Author</th>
                <th>Is Finished?</th>
                <th>Comments</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
            @forelse($tasks as $task)
                <tr class="">
                    <td>{{$task->id}}</td>
                    <td>{{\Illuminate\Support\Str::limit($task->name,22)}}</td>

                    <td>{{$task->priority->status}} <button class="btn {{$task->priority->label}}"></button></td>
                    <td>
                        {{$task->creator->name}}
                    </td>
                    <td>
                        {{$task->is_finished == 1 ? 'Task Finished' : 'Task is Active'}}
                    </td>

                    <td>{{$task->comments->count()}}</td>
                    <td>{{$task->created_at->diffForHumans()}}</td>


                    <td>
                        <a href="{{route('task.show', $task->id)}}" target="_blank">
                            <button class="btn btn-info btn-sm">View</button>
                        </a>
                        <a href="{{route('admin.task.edit', $task->id)}}">
                            <button class="btn btn-primary btn-sm">Edit</button>
                        </a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                data-target="#modal-danger-{{$task->id}}">
                            Delete
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$task->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete Task?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.task.destroy',$task->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>

                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
               No Tasks Yet
            @endforelse
            </tbody>
        </table>
        {{$tasks->links()}}
    </div>
    <!-- /.modal -->
@stop
