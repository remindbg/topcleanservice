
@extends('rpanel.layouts.app')

@section('title','New Topic')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">New Topic</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{route('rpanel.topic.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-7">
                            @method('post')

                            <div class="card card-success">
                                <div class="card-header">
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-minus"></i></button>
                                        <button type="button" class="btn btn-tool"
                                                data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                    <h3 class="card-title">General</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <small>Name</small>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" value=""
                                                       name="name">

                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <p>SLUG (editable after)</p>
                                            <div class="input-group mb-3">


                                                <input type="text" class="form-control" value=""
                                                       name="slug" disabled>

                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="row">


                                                <div class="col-lg-10">
                                                    <div class="form-group">
                                                        <label for="exampleInputFile">Image</label>
                                                        <div class="input-group">
                                                            <div class="custom-file">
                                                                <input type="file" class="custom-file-input"
                                                                       name="cat_image"
                                                                       id="exampleInputFile">
                                                                <label class="custom-file-label"
                                                                       for="exampleInputFile">File</label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label> Description</label>
                                                        <textarea class="form-control"
                                                                  rows="3" name="description" id="body"></textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <hr>
                                <button type="submit" class="btn btn-block btn-info btn-lg">Create</button>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="card card-blue collapsed-card">
                                <div class="card-header ">
                                    <div class="card-tools">
                                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                                class="fas fa-plus"></i></button>
                                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                                    </div>
                                    <h3 class="card-title">SEO</h3>
                                </div>
                                <div class="card-body">
                                    <div class="input-group mb-3">

                                    </div>

                                    <div class="form-group">
                                        <label>SEO Title</label>
                                    </div>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="" name="seo_title"
                                        >
                                    </div>

                                    <div class="form-group">
                                        <label>SEO Description</label>
                                        <textarea class="form-control" rows="3"
                                                  name="seo_description"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
@stop

