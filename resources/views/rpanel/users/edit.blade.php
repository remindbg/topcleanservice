@extends('admin.layouts.app')


@section('content')

    <h5 class="mb-2">Update User</h5>

    <div class="card-body">
        <form action="{{route('admin.user.update',$user->id)}}" method="POST">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    @method('put')

                    <div class="card card-success">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool"
                                        data-card-widget="remove"><i class="fas fa-times"></i></button>
                            </div>
                            <h3 class="card-title">General Information</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label>User Name</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$user->name}}"
                                               name="name">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <label>User Email Adress</label>
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" value="{{$user->email}}"
                                               name="email">

                                    </div>
                                </div>



                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Is Admin?</label>
                                        <select class="form-control" name="is_admin">

                                                <option value="1" {{$user->is_admin ? 'selected' : ''}}>Admin</option>
                                                <option value="0" {{$user->is_admin ? '' : 'selected'}}>Not
                                                    Admin</option>


                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-block btn-info btn-lg">Update User</button>
                    </div>
                </div>

            </div>
        </form>

    </div>

@stop



@section('scripts')



@endsection


@section('css')


@endsection
