@extends('admin.layouts.app')

@section('content')
    <div class="card-body table-responsive p-0">

        <hr>
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>name</th>
                <th>email</th>
                <th>is Admin?</th>
                <th>Comments</th>
                <th>Tasks</th>
                <th>Date</th>
                <th>Action</th>
            </tr>
            @forelse($users as $user)
                <tr class="">
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>

                    <td>
                        {{$user->is_admin == 1 ? 'Admin' : 'Not Admin'}}
                    </td>

                    <td>{{$user->comments->count()}}</td>
                    <td>{{$user->tasks->count()}}</td>


                    <td>
                        <a href="{{route('user.show', $user->id)}}" target="_blank">
                            <button class="btn btn-info btn-sm">View</button>
                        </a>
                        <a href="{{route('admin.user.edit', $user->id)}}">
                            <button class="btn btn-primary btn-sm">Edit</button>
                        </a>
                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal"
                                data-target="#modal-danger-{{$user->id}}">
                            Delete
                        </button>
                    </td>
                </tr>
                <div class="modal fade" id="modal-danger-{{$user->id}}">
                    <div class="modal-dialog">
                        <div class="modal-content bg-danger">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete User?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>

                            <div class="modal-footer justify-content-between">
                                <form action="{{route('admin.user.destroy',$user->id)}}" method="POST">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Cancel</button>

                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>

            @empty
                <hr>
                No Users Yet
            @endforelse
            </tbody>
        </table>
        {{$users->links()}}
    </div>
    <!-- /.modal -->
@stop
