@extends('layouts.app')

@section('metas')
    <title>{{$service->seo_title ?? $service->name }}</title>

    <meta name="description" content="{{$service->seo_title ?? $service->seo_description}}">
@stop

@section('content')
    <div id="page-banner" class="page-banner">
        <img src="{{asset('build/images/page-banner.png')}}" alt="page-banner">
        <!-- container -->
        <div class="page-detail">
            <div class="container">
                <h3 class="page-title">{{$service->name}}</h3>
                <div class="page-breadcrumb pull-right">
                    <ol class="breadcrumb">
                        <li><a title="Home" href="/">Home</a></li>
                        <li><a title="Services" href="service.html">Services</a></li>
                        <li class=""><a title="{{$service->name}}" href="{{$service->urlslug()}}">{{$service->name}}</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- container /- -->
    </div>


    <div id="service-section" class="service-section ow-section services-style2">
        <!-- container -->
        <div class="container">
            <!-- col-md-3 -->

            <!-- col-md-9 -->
            <div class="col-md-9 col-sm-8 services-content-area">
                <!-- Section Header -->
                <div class="section-header">
                    <h1>{{$service->title}}</h1>
                </div><!-- Section Header /- -->

                <p>
                    {!! $service->body !!}
                </p>

                <a title="Get A Quote" href="/contact" class="btn btn-block">Schedule your {{$service->name}} today!</a>
            </div>
            <div class="col-md-3 col-sm-4 content-sidebar">
                <aside class="widget service-category-widget">
                    <h3>Other Services in London</h3>
                    <ul>
                        @forelse($services as $service)
                            <li>
                                <i class="fa fa-long-arrow-right"></i>
                                <a title="{{$service->name}}" href="{{$service->urlslug()}}">{{$service->name}}</a>
                            </li>

                        @empty

                        @endforelse
                    </ul>
                </aside>
                <aside class="widget address-widget">
                    <h3>MORE INFORMATION</h3>
                    <p>123 Cleaning Company,</p>
                    <p>New Street CA 7854, Park Avenue</p>
                    <p>Sydney 25.</p>
                    <p><span>+(01) 800 527 4800</span></p>
                    <a title="Mail To" href="mailto:makeclean@example.com">Makeclean@example.com</a>
                </aside>
            </div><!-- col-md-3 /- -->
        </div><!-- container /- -->
    </div>

@stop
