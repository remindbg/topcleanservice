@extends('layouts.app')

@section('content')

    <div id="product-section" class="product-section shop">
        <!-- container -->
        <div class="container">
            <div class="row">
                <!-- col-md-3 -->
                <div class="col-md-3 col-sm-4 product-sidebar">
                    <aside class="widget service-category-widget">
                        <h3>category menu</h3>
                        <ul>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Bathroom Cleaners" href="#">BATHROOM CLEANERS</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Equipment" href="#">Equipment</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Laundry &amp; Linen" href="#">Laundry &amp; linen</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Mopping Equipment" href="#">mops / mopping equipment</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Paper Product" href="#">paper products</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Trash Receptacles" href="#">trash receptacles / carts</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Window Washing" href="#">window washing</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Chemicals" href="#">chemicals</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Vacuum Cleaners" href="#">vacuum cleaners</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Cleaning Tools" href="#">CLEANING TOOLS</a></li>
                            <li><i class="fa fa-long-arrow-right"></i><a title="Brooms &amp; Hand Pads" href="#">Brooms &amp; Hand Pads</a></li>
                        </ul>
                    </aside>

                    <aside class="widget widget_search">
                        <h3>SEARCH PRODUCT</h3>
                        <form method="get" action="#" role="search" class="search">
                            <input type="text" required="" class="form-control" placeholder="FIND HERE..." id="s" name="s">
                            <span class="search-icon input-group-btn"><button type="submit" class="btn btn-xlg"></button></span>
                        </form>
                    </aside>

                    <aside class="widget price-filter-widget">
                        <h3>Price Filter</h3>
                        <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 66.6667%;"></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 0%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 66.6667%;"></span></div>
                        <a title="filter" href="#" class="filter">Filter</a>
                        <div class="price-input">
                            <label>Price:</label>
                            <span id="amount">$0</span>
                            <label> - </label>
                            <span id="amount2"> $10000</span>
                        </div>
                    </aside>

                    <aside class="widget add-widget">
                        <a title="Add Banner" href="#"><img src="{{asset('build/images/product/add-banner.jpg')}}" alt="add banner"></a>
                    </aside>
                </div><!-- col-md-3 /- -->
                <!-- col-md-9 -->
                <div class="col-md-9 col-sm-8">
                    <div class="product-category">
                        <h4>All Product</h4>
                        <ul>
                            <li><a title="New Product" href="#">New Product</a></li>
                            <li><a title="Best Seller" href="#">best seller</a></li>
                            <li><a title="Top Rated" href="#">top rated</a></li>
                            <li><a title="Featured Item" href="#">featured item</a></li>
                        </ul>
                    </div>
                    <ul class="product-main row">
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product Image" href="#">
                                    <img src="build/images/product/product-1.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">Method Squirt + Mop Hard Floor Cleaner</a>
                                    <span>
									<span class="price">$ 65.50</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-2.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">Scotch Brite Heavy Duty Scouring Pads 20 Count </a>
                                    <span>
									<span class="price">$ 45.70</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-3.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">Akro-Mils 09185 Red Plastic Tote Caddy </a>
                                    <span>
									<span class="price">$ 72.50</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-4.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">remium Multipurpose Gloves </a>
                                    <span>
									<span class="price">$ 95.00</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-5.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">Swiffer Sweeper Wet Mopping Cloth Refills</a>
                                    <span>
									<span class="price">$ 36.50</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-6.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">Mop &amp; Glo Triple Action Floor Shine Cleaner</a>
                                    <span>
									<span class="price">$ 55.10</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-7.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">Resolve Deep Clean Powder</a>
                                    <span>
									<span class="price">$ 45.40</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-8.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">Microfiber Magic Sponge - 3 Piece Pack </a>
                                    <span>
									<span class="price">$ 31.10</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-6">
                            <div class="product-box">
                                <a title="Product" href="#">
                                    <img src="build/images/product/product-9.jpg" alt="product">
                                    <div class="product-hover">
									<span class="rating">
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
										<i class="fa fa-star-o"></i>
									</span>
                                        <ul>
                                            <li>quick view</li>
                                            <li><i class="fa fa-heart-o"></i></li>
                                            <li><img src="build/images/icon/repeat-icon.png" alt="repeat icon"></li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="product-detail">
                                    <a title="Product Name" href="#" class="product-title">SAndex Original Glass Cleaner, 26 fl oz</a>
                                    <span>
									<span class="price">$ 26.70</span>
									<a title="Add To Cart" href="#" class="btn pull-right">Add to Cart</a>
								</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <a href="#" class="btn">Show More</a>
                    <div class="pull-right">
                        <ul class="pagination">
                            <li><a title="Pagination" href="#">1</a></li>
                            <li><a title="Pagination" href="#">2</a></li>
                            <li>
                                <a title="Pagination" href="#" aria-label="Next">
                                    <i class="fa fa-long-arrow-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div><!-- col-md-9 /- -->
            </div>
        </div><!-- container /- -->
    </div>

    @endsection
