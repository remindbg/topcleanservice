<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/blog', function () {
    return view('articles.index');
});
Route::get('/shop', function () {
    return view('shop.index');
});
Auth::routes();
Route::get('cleaning-articles/{id}/{cat_slug}', 'CategoryController@single')->name('category.single');

Route::get('/', 'HomeController@index')->name('home');
Route::get('cleaning-articles', 'ArticleController@index')->name('blog.index');
Route::get('cleaning-articles/{id}/{cat_slug}/{article_slug}', 'ArticleController@single')->name('blog.single');



Route::get('london-services/{id}/{slug}', 'ServicesController@single')->name('services.single');









Route::namespace('Admin')->prefix('myrpanel')->middleware('isAdmin')->name('rpanel.')->group(function () {

    Route::get('/', function () {
        return view('rpanel.layouts.app');
    })->name('welcome');

    Route::resource('article','ArticleController');
    Route::resource('category','CategoryController');
    Route::resource('services','ServiceController');

    Route::post('uploadimage', 'ArticleController@uploadImage')->name('uploadimage');


});
